package ictgradschool.industry.lab06.ex01;

/**
 * Should represent a Sphere.
 *
 * TODO Implement this class.
 */
public class Sphere {

        double radius;
        double[] coordinates = new double[3];
        int count = 0;



        public double getRadius() {
            return radius;
        }

        public double[] getCoordinates() {
            return coordinates;
        }

        public double surfaceArea() {
            radius = getRadius();
            double surfaceArea = 4 * (Math.PI * (Math.pow(radius, 2.0)));
            return surfaceArea;
        }
        public double volume() {
            radius = getRadius();
            double volume = (4 / 3) * (Math.PI * (Math.pow(radius, 3.0)));
            return volume;
        }

        public int getCount() {
            return count;
        }

}
